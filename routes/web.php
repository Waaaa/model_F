<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('account', 'AccountController@store');
Route::get('account/{accountID}', 'AccountController@show');

Route::post('album', 'AlbumController@store');
Route::get('album/{albumID}', 'AlbumController@show');
Route::patch('album/{albumID}', 'AlbumController@update');
Route::delete('album/{albumID}', 'AlbumController@delete');
Route::post('album/{albumID}/image', 'AlbumController@image');
