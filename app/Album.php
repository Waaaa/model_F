<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Album extends Model
{
	public $incrementing = false;
    public function owner(){
    	return $this->belongsTo('App\Account', 'account_id');
    }
    public function images(){
    	return $this->hasMany('App\Image');
    }

}
