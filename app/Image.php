<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	public $incrementing = false;
    public function owner(){
    	return $this->belongs('App\Album');
    }
}
