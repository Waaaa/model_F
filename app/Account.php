<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Account extends Model
{
	public $incrementing = false;
    public function albums(){
    	return $this->hasMany('App\Album');
    }
}
