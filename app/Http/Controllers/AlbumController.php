<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \SimpleXMLElement;
use \DOMDocument;
use App\Account;
use App\Album;
use App\Image;

class AlbumController extends Controller
{
    public function store(Request $request){
    	$xml = simplexml_load_string($request->getContent());
    	$token = str_replace('Token ', '', $request->header('Authorization'));
    	$account = Account::where('accountToken',$token)->first();
    	$album_title_check = isset($xml->title);  
        
    	// || !isset($xml->title) || !isset($xml->description)  	
    	if(! isset($account->id) || !isset($xml->title) || !isset($xml->description) ){
    		$success_value = "0";
    		$status_value = "400";
    		$message = '資料錯誤';
    	}else{
    		$albumToken = "";
    		$len = rand(5,11);
    		for($i = 0 ; $i < $len ; $i++){
    			if(rand(0,1)){
    				if(rand(0,1)){
    					$char = chr(rand(97,122));
    				}else{
    					$char = chr(rand(65,90));
    				}
    				$albumToken = $albumToken.$char;
    			}else{
    				$num = rand(1,9);
    				$albumToken = $albumToken.strval($num);
    			}
    		}
    		$album = new Album();
            $album->id = $albumToken;
    		$album->title = $xml->title;
    		$album->description = $xml->description;
    		$album->account_id = $account->id; 		
    		$album->save();
    		$success_value = "1";
    		$status_value = "200";
    		$message = $albumToken;
    	}
    	
    	//建立回應的XML文件
    	$response_xml = new DOMDocument('1.0', 'utf-8');	
		$data = $response_xml->createElement("data" ,$message);
		$type = $response_xml->createAttribute("type");
		$type->value = "string";
		$data->appendChild($type);
		$success = $response_xml->createAttribute("success");
		$success->value = $success_value;
		$data->appendChild($success);
		$status = $response_xml->createAttribute("status");
		$status->value = $status_value;
		$data->appendChild($status);
		$response_xml->appendChild($data);
		//$response_xml->saveXML()
    	return response($response_xml->saveXML())
           ->header('Content-Type', 'application/xml');

    }
    public function show(Request $request, $albumID){
    	$token = str_replace('Token ', '', $request->header('Authorization'));
        $account_token = Account::where('accountToken',$token)->first();
        $album = Album::where('id', $albumID)->first();
        if(isset($account_token->id) && isset($album->id)){
            $response_xml = new DOMDocument('1.0', 'utf-8');    
            $data = $response_xml->createElement("data");
            $success = $response_xml->createAttribute("success");
            $success->value = '1';
            $data->appendChild($success);
            $status = $response_xml->createAttribute("status");
            $status->value = '200';
            $data->appendChild($status);

            $id = $response_xml->createElement('id', $album->id);
            $title = $response_xml->createElement('title', $album->title);
            $description = $response_xml->createElement('description', $album->description);
            $datetime = $response_xml->createElement('datetime', strtotime($album->created_at));
            $data->appendChild($id);
            $data->appendChild($title);
            $data->appendChild($description);
            $data->appendChild($datetime);
            /* 跳過covers */
            $account = $album->owner;     
            $account_e = $response_xml->createElement('account', $account->id);
            $link = $response_xml->createElement('link', 'http://127.0.0.1/RESTful/public/album/'.$album->id);
            $data->appendChild($account_e);
            $data->appendChild($link);

            /* 處理image */
            $images = $album->images;
            $images_count = $response_xml->createElement('images_count', $images->count());
            $data->appendChild($images_count);
            $img = $response_xml->createElement('images');
            foreach ($images as $i) {
                $item = $response_xml->createElement('item');
                $img_id = $response_xml->createElement('id', $i->id);
                $img_title = $response_xml->createElement('title', $i->title);
                $img_description = $response_xml->createElement('description', $i->description);
                $img_datetime = $response_xml->createElement('datetime', strtotime($i->created_at));
                $info = getimagesize(public_path().'/images/'.$i->image);
                $img_w = $response_xml->createElement('width', $info[0]);
                $img_h = $response_xml->createElement('height', $info[1]);
                $img_s = $response_xml->createElement('size', filesize(public_path().'/images/'.$i->image));
                $img_link = $response_xml->createElement('link', 'http://127.0.0.1/RESTful/public/album/i/'.$i->image);
                $item->appendChild($img_id);
                $item->appendChild($img_title);
                $item->appendChild($img_description);
                $item->appendChild($img_datetime);
                $item->appendChild($img_w);
                $item->appendChild($img_h);
                $item->appendChild($img_s);
                $item->appendChild($img_link);
                $img->appendChild($item);
            }
            $data->appendChild($img);
            $response_xml->appendChild($data);
            return response($response_xml->saveXML())
                ->header('Content-Type', 'application/xml');
        }else{
            $response_xml = '<?xml version="1.0" encoding="UTF-8"?><data success="0" status="404" />';
            return response($response_xml)
                ->header('Content-Type', 'application/xml');
        }

    }
    public function update(Request $request, $albumID){
        $token = str_replace('Token ', '', $request->header('Authorization'));
        $account_token = Account::where('accountToken',$token)->first();
        $album = Album::where('id', $albumID)->first();
        $xml = simplexml_load_string($request->getContent());
        if(isset($account_token->id) && isset($album->id)){
            if(isset($xml->title)){
                $album->title = $xml->title;
            }
            if(isset($xml->description)){
                $album->description = $xml->description;
            }
            $album->save();
            $response_xml = '<?xml version="1.0" encoding="UTF-8"?><data success="1" status="200" />';
            return response($response_xml)
                ->header('Content-Type', 'application/xml');
        }
    }
    public function delete(Request $request, $albumID){
    	$token = str_replace('Token ', '', $request->header('Authorization'));
        $account_token = Account::where('accountToken',$token)->first();
        $album = Album::where('id', $albumID)->first();
        if(isset($account_token->id) && isset($album->id)){
            $images = $album->images;
            $path = public_path().'/images/';
            foreach ($images as $img) {
                unlink($path.$img->image);
                $img->delete();
            }
            $album->delete();
        }else{
            $response_xml = '<?xml version="1.0" encoding="UTF-8"?><data success="0" status="401" />';
            return response($response_xml)
                ->header('Content-Type', 'application/xml');
        }
    }
    public function image(Request $request, $albumID){
        //return response($request->title);
        $album = Album::where('id',$albumID)->first();
        if(isset($album->id)){
        	if ($request->hasFile('image')) {
                $token = "";
                for($i = 0 ; $i < 10 ; $i++){
                    if(rand(0,1)){
                        if(rand(0,1)){
                            $char = chr(rand(97,122));
                        }else{
                            $char = chr(rand(65,90));
                        }
                        $token = $token.$char;
                    }else{
                        $num = rand(1,9);
                        $token = $token.strval($num);
                    }
                }

                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $file_name = $token.'.'.$extension;
                $path = public_path() . '/images/';
                $upload_success = $file->move($path, $file_name);
                $info = getimagesize($path.$file_name);
                
                $image = new Image();
                $image->id = $token;
                $image->title = $request->title;
                $image->description = $request->description;
                $image->image = $file_name;
                $image->album_id = $album->id;
                $image->save();

                $response_xml = new DOMDocument('1.0', 'utf-8');    
                $data = $response_xml->createElement("data");
                $success = $response_xml->createAttribute("success");
                $success->value = '1';
                $data->appendChild($success);
                $status = $response_xml->createAttribute("status");
                $status->value = '200';
                $data->appendChild($status);

                $datetime = $response_xml->createElement("datetime", strtotime($image->created_at));
                $width = $response_xml->createElement("width", $info[0]);
                $height = $response_xml->createElement("height", $info[1]);
                $size = $response_xml->createElement("size", filesize($path.$file_name));
                $data->appendChild($datetime);
                $data->appendChild($width);
                $data->appendChild($height);
                $data->appendChild($size);
                $response_xml->appendChild($data);
                //$response_xml->saveXML()
                return response($response_xml->saveXML())
                   ->header('Content-Type', 'application/xml');
            }
        }
    }
}
