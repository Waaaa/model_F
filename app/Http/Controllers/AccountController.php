<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \SimpleXMLElement;
use \DOMDocument;
use App\Account;
class AccountController extends Controller
{
    public function store(Request $request){
    	$bodyContent = $request->getContent();
    	$xml = simplexml_load_string($bodyContent);
    	//$token = $request->header('Authorization');
    	$check = Account::where('account', $xml->account)->get();
    	
    	if(isset($check->id)){
    		$success_value = "0";
    		$status_value = "400";
    		$message = "此帳號已經被註冊";
    	}else{
    		$token = "";
    		for($i = 0 ; $i < 7 ; $i++){
    			if(rand(0,1)){
    				if(rand(0,1)){
    					$char = chr(rand(97,122));
    				}else{
    					$char = chr(rand(65,90));
    				}
    				$token = $token.$char;
    			}else{
    				$num = rand(1,9);
    				$token = $token.strval($num);
    			}
    		}
            $id = "";
            for($i = 0 ; $i < 7 ; $i++){
                if(rand(0,1)){
                    if(rand(0,1)){
                        $char = chr(rand(97,122));
                    }else{
                        $char = chr(rand(65,90));
                    }
                    $id = $id.$char;
                }else{
                    $num = rand(1,9);
                    $id = $id.strval($num);
                }
            }


    		$account = new Account();
            $account->id = $id;
    		$account->account = $xml->account;
    		$account->bio = $xml->bio;
    		$account->accountToken = $token;
    		$account->save();

    		$success_value = "1";
    		$status_value = "200";
    		$message = $id;
    	}
    	
    	//建立回應的XML文件
    	$response_xml = new DOMDocument('1.0', 'utf-8');	
		$data = $response_xml->createElement("data" ,$message);
		$type = $response_xml->createAttribute("type");
		$type->value = "string";
		$data->appendChild($type);
		$success = $response_xml->createAttribute("success");
		$success->value = $success_value;
		$data->appendChild($success);
		$status = $response_xml->createAttribute("status");
		$status->value = $status_value;
		$data->appendChild($status);
		$response_xml->appendChild($data);
		//$response_xml->saveXML()
    	return response($response_xml->saveXML())
           ->header('Content-Type', 'application/xml')
           ->header('Authorization', 'Token '.$token);
    }
    public function show(Request $request, $accountID){
    	$token = str_replace('Token ', '', $request->header('Authorization'));
    	$account_token = Account::where('accountToken',$token)->first();
    	$account = Account::where('id', $accountID)->first();
    	if(!isset($account_token->id) || !isset($account->id)){
    		$response_xml = '<?xml version="1.0" encoding="UTF-8"?><data success="0" status="404" />';
    		return response($response_xml)
           		->header('Content-Type', 'application/xml');
    	}else{
    		$response_xml = new DOMDocument('1.0', 'utf-8');	
			$data = $response_xml->createElement("data");
			$type = $response_xml->createAttribute("type");
			$type->value = "string";
			$data->appendChild($type);
			$success = $response_xml->createAttribute("success");
			$success->value = '1';
			$data->appendChild($success);
			$status = $response_xml->createAttribute("status");
			$status->value = '200';
			$data->appendChild($status);
			$account_e = $response_xml->createElement("account", $account->account);
			$bio_e = $response_xml->createElement("bio", $account->bio);
			$created_e = $response_xml->createElement("created", strtotime($account->created_at));
			$data->appendChild($account_e);
			$data->appendChild($bio_e);
			$data->appendChild($created_e);
			$response_xml->appendChild($data);
			$albums = $response_xml->createElement("albums");
			$data->appendChild($albums);

			$account_albums = $account->albums;
			foreach ($account_albums as $a) {
				$album = $response_xml->createElement("album");
				$id = $response_xml->createAttribute("id");
				$id->value = $a->id;
				$count = $response_xml->createAttribute("count");
				$count->value = $a->images->count();
				$album->appendChild($id);
				$album->appendChild($count);
				$albums->appendChild($album);
			}
			return response($response_xml->saveXML())
           		->header('Content-Type', 'application/xml');

    	}
    }
    public function update($accountID){
    	
    }
}
