# laravel RESTful API
第48屆全國技能競賽暨第48屆國際技能競賽國手選拔賽模擬試題，設計一個相簿管理RESTfulAPI，並採用標準的XML/JSON/其他格式回傳。

## API功能
| 功能      | URL                    | Method | Request | Respone             |      |
| ------- | ---------------------- | ------ | ------- | ------------------- | ---- |
| 新增使用者   | /account               | POST   | 使用者資訊   | 使用者ID/回傳錯誤訊息/Token(在header中回傳) | done |
| 取得使用者資訊 | /account/{accountID}   | GET    | -       | 使用者資訊               | done |
| 建立相簿    | /album                 | POST   | 相簿資訊    | 相簿ID                | done |
| 查詢特定相簿  | /album/{albumID}       | GET    | -       | 相簿資訊                | done |
| 更新相簿    | /album/{albumID}       | PATCH  | 相簿資訊    | 更新成功/失敗資訊           | done |
| 刪除相簿    | /album/{albumID}       | DELETE | -       | 更新成功/失敗資訊           | done |
| 上傳圖片    | /album/{albumID}/image | POST   | 照片資訊    | 照片 ID               | done |

## 資料庫規劃
```SQL
CREATE DATABASE IF NOT EXISTS `restful` DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `accounts` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `account` varchar(10) NOT NULL,
    `bio` text,
    `accountToken` char(7) NOT NULL,
    `created_at` datetime(0) DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime(0) DEFAULT NULL,
    PRIMARY KEY(`id`),
    UNIQUE(`accountToken`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='使用者資料表';

CREATE TABLE IF NOT EXISTS `albums` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) NOT NULL,
    `desctiption` text,
    `account_id` int(11) NOT NULL,
    `created_at` datetime(0) DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime(0) DEFAULT NULL,
    PRIMARY KEY(`id`),
    CONSTRAINT accountFK FOREIGN KEY(`account_id`) REFERENCES `accounts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='相簿資料表';

CREATE TABLE IF NOT EXISTS `images`(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) NOT NULL,
    `description` text,
    `image` varchar(20) NOT NULL,
    `album_id` int(11) NOT NULL,
    `created_at` datetime(0) DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime(0) DEFAULT NULL,
    PRIMARY KEY(`id`),
    CONSTRAINT albumFK FOREIGN KEY(`album_id`) REFERENCES `albums`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='相片資料表';
```

## laravel 程式撰寫
**關閉CSRF**
```PHP
// C:\xampp\htdocs\RESTful\app\Http\Middleware\VerifyCsrfToken.php
public function handle($request, Closure $next)
{
    // 使用CSRF
    //return parent::handle($request, $next);
    // 禁用CSRF
    return $next($request);
}
```

**建立路由**
```PHP
// C:\xampp\htdocs\RESTful\routes\web.php
Route::post('account', 'AccountController@store');
Route::get('account/{accountID}', 'AccountController@show');

Route::post('album', 'AlbumController@store');
Route::post('album/{albumID}', 'AlbumController@show');
Route::patch('album/{albumID}', 'AlbumController@update');
Route::delete('album/{albumID}', 'AlbumController@delete');
Route::post('album/{albumID}/image', 'AlbumController@image');
```

**連接資料庫**
```PHP
// .env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=restful
DB_USERNAME=admin
DB_PASSWORD=1234
```

**建立migration**
```PHP
$ php artisan make:migration [tablename]


//以accounts當語法範例，詳細資料表規格參考最上方
class Accounts extends Migration
{
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->string('id')->primaty();
            $table->string('account');
            $table->string('bio');
            $table->string('accountToken');
            $table->timestamps();
        });
    }
    public function down()
    {
         Schema::drop('accounts');
    }
}
```
**執行migration**
```PHP
$ php artisan migrate
```


**建立model**
```PHP
$ php artisan make:model [modelname]
```

**取消自動遞增**
```PHP
public $incrementing = false;
```

**定義關聯**

* Account → Album : 1對多
* Album → Image : 1對多
```PHP
//C:\xampp\htdocs\RESTful\app\Account.php
class Account extends Model
{
    public function albums(){
            return $this->hasMany('App\Album');
    }
}

//C:\xampp\htdocs\RESTful\app\Album.php
class Album extends Model
{
    public function owner(){
            return $this->belongs('App\Account');
    }
    public function images(){
            return $this->hasMany('App\Image');
    }
}

//C:\xampp\htdocs\RESTful\app\Image.php
class Image extends Model
{
    public function owner(){
            return $this->belongs('App\Album');
    }
}
```

**關聯使用示範**
```PHP
$account = Account::where('id', $accountID)->first();
$account_albums = $account->albums;
foreach ($account_albums as $a) {
      /* .. */
}
```

**建立Controller**
```PHP
$ php artisan make:controller [controllername]
```

**取得request的內容並將文字轉換成XML**
```PHP
$bodyContent = $request->getContent();
$xml = simplexml_load_string($bodyContent);
```

**取得header的資訊**
```PHP
$request->header('Authorization');
```

**檔案上傳**
```PHP
$file = $request->file('image');
$extension = $file->getClientOriginalExtension();
$file_name = strtotime('now').$extension;
$path = public_path() . '/images/';
$upload_success = $file->move($path, $file_name);
```

**回傳回應**
```PHP
return response($response_xml->saveXML())->header('Content-Type', 'application/xml');

```